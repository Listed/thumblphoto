//
//  SAThumblTableViewCell.swift
//  ThumblPhoto
//
//  Created by admin on 28.01.2018.
//  Copyright © 2018 SasinAndrey. All rights reserved.
//

import UIKit

class SAThumblTableViewCell: UITableViewCell {
    
    let imageViewAvatar = UIImageView()
    let labelUser = UILabel()
    let buttonRead = UIButton()
    let mainImage = UIImageView()
    let labelDescription = UILabel()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        let marginGuide = contentView.layoutMarginsGuide
        
        //Setup Avatar Image
        contentView.addSubview(imageViewAvatar)
        imageViewAvatar.backgroundColor = #colorLiteral(red: 0.5577817559, green: 0.1720042825, blue: 0.8051664829, alpha: 1)
        imageViewAvatar.layer.cornerRadius = 50 / 2.0
        imageViewAvatar.layer.masksToBounds = true
        
        imageViewAvatar.translatesAutoresizingMaskIntoConstraints = false
        imageViewAvatar.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        imageViewAvatar.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        imageViewAvatar.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageViewAvatar.widthAnchor.constraint(equalToConstant: 50.0).isActive = true
        
        //Setup label users
        contentView.addSubview(labelUser)
        labelUser.text = "user name"
        labelUser.numberOfLines = 0
        
        labelUser.translatesAutoresizingMaskIntoConstraints = false
        labelUser.leadingAnchor.constraint(equalTo: imageViewAvatar.trailingAnchor, constant: 10.0) .isActive = true
        labelUser.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        labelUser.heightAnchor.constraint(equalToConstant: 50).isActive = true
        labelUser.widthAnchor.constraint(greaterThanOrEqualToConstant: 50.0).isActive = true
        
        //Setup button read
        contentView.addSubview(buttonRead)
        buttonRead.setTitle("Read", for: .normal)
        buttonRead.setTitleColor(#colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), for: .normal)
        
        buttonRead.translatesAutoresizingMaskIntoConstraints = false
        buttonRead.leadingAnchor.constraint(equalTo: labelUser.trailingAnchor, constant: 10.0) .isActive = true
        buttonRead.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        buttonRead.heightAnchor.constraint(greaterThanOrEqualToConstant: 50.0).isActive = true
        buttonRead.widthAnchor.constraint(greaterThanOrEqualToConstant: 50.0).isActive = true
        
        //Setup Main image
        contentView.addSubview(mainImage)
        //mainImage.image = #imageLiteral(resourceName: "1.png")
        mainImage.contentMode = .scaleAspectFill
        mainImage.clipsToBounds = true
        
        mainImage.translatesAutoresizingMaskIntoConstraints = false
        mainImage.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor, constant: -15.0).isActive = true
        mainImage.topAnchor.constraint(equalTo: imageViewAvatar.bottomAnchor, constant: 10.0).isActive = true
        mainImage.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor, constant: 15.0).isActive = true
        mainImage.heightAnchor.constraint(greaterThanOrEqualToConstant: 100).isActive = true
        
        //Setup label description
        contentView.addSubview(labelDescription)
        labelDescription.text = "temp"
        labelDescription.textAlignment = .center
        labelDescription.numberOfLines = 0
        //labelDescription.sizeToFit()
        
        labelDescription.translatesAutoresizingMaskIntoConstraints = false
        labelDescription.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        labelDescription.topAnchor.constraint(equalTo: mainImage.bottomAnchor, constant: 20.0).isActive = true
        labelDescription.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        labelDescription.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

