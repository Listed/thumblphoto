//
//  SANetworkManager.swift
//  ThumblPhoto
//
//  Created by admin on 28.01.2018.
//  Copyright © 2018 SasinAndrey. All rights reserved.
//

import UIKit
import Alamofire

class SANetworkManager: NSObject {
    
    let service = "myService"
    let account = "myAccount"
    
    func getRequestFromPost(textForSearch text: String, completionHandler: @escaping (_ responseRequest:NSMutableArray?) ->Void){
        
        let retrievedPassword : String?
        
        if let str = KeychainService.loadPassword(service: service, account: account) {
            retrievedPassword = str
            
            Alamofire.request(
                URL(string: "http://api.tumblr.com/v2/tagged")!,
                method: .get,
                parameters: ["tag": "\(text)",
                    "filter":"text",
                    "api_key":retrievedPassword!]).responseJSON { (response) in
                        response.result.ifSuccess {
                            let arraysThumbls = NSMutableArray()
                            
                            if let dictResponse = response.result.value as? NSDictionary{
                                if let arrayFromResponseKeys = dictResponse.object(forKey: "response") as? NSArray{
                                    if arrayFromResponseKeys.count > 0{
                                        for objectThumbl in arrayFromResponseKeys {
                                            if let object = objectThumbl as? NSDictionary{
                                                
                                                if ((object.object(forKey: "type") as? String)?.elementsEqual("photo"))!{
                                                    let objetThumbl = SAThumblObjectParser.init(thumblPhoto: object)
                                                    
                                                    arraysThumbls.add(objetThumbl)
                                                }
                                            }
                                        }
                                    }
                                    return completionHandler(arraysThumbls)
                                }
                            }
                            
                        }
                        return completionHandler(nil)
            }
        }
        else {print("Password does not exist")}
    }
}

