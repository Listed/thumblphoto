//
//  SAThumblObjectParser.swift
//  ThumblPhoto
//
//  Created by admin on 28.01.2018.
//  Copyright © 2018 SasinAndrey. All rights reserved.
//

import UIKit

class SAThumblObjectParser: NSObject {
    
    var type: String?
    var blog_name : String?
    var summary : String?
    var originalSize : NSDictionary!
    
    init(thumblPhoto objectThumblPhoto:NSDictionary) {
        if let type = objectThumblPhoto.object(forKey: "type") as? String{
            self.type = type
        }
        if let blog_name = objectThumblPhoto.object(forKey: "blog_name") as? String{
            self.blog_name = blog_name
        }
        if let summary = objectThumblPhoto.object(forKey: "summary") as? String{
            self.summary = summary
        }
        if let photos = objectThumblPhoto.object(forKey: "photos") as? NSArray{
            self.originalSize = (photos.firstObject as! NSDictionary).object(forKey: "original_size") as! NSDictionary
        }
    }
}


