//
//  SAImageViewController.swift
//  ThumblPhoto
//
//  Created by admin on 28.01.2018.
//  Copyright © 2018 SasinAndrey. All rights reserved.
//

import UIKit
import AlamofireImage

class SAImageViewController: UIViewController,UIScrollViewDelegate {
    var poster: SAThumblObjectParser! = nil
    
    let scrollView: UIScrollView = {
        let v = UIScrollView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.minimumZoomScale = 1.0
        v.maximumZoomScale = 6.0
        v.backgroundColor = UIColor(displayP3Red: 0/255, green: 150/255, blue: 136/255, alpha: 1.0)
        return v
    }()
    
    let imageView : UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.tintColor = UIColor.white
        
        setUpUI()
        
        title = poster.blog_name
        
        // add the scroll view to self.view
        self.view.addSubview(scrollView)
        scrollView.delegate = self
        
        // constrain the scroll view to 8-pts on each side
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 8.0).isActive = true
        scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 8.0).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -8.0).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -8.0).isActive = true
        
        // add the uiimage view to scrollView
        scrollView.addSubview(imageView)
        
        let urlImage = URL(string: poster.originalSize.object(forKey: "url") as! String)!
        imageView.af_setImage(withURL: urlImage)
        
        // constrain the image view to 0-pts on each side
        imageView.leftAnchor.constraint(equalTo: scrollView.leftAnchor).isActive = true
        imageView.topAnchor.constraint(equalTo: scrollView.topAnchor).isActive = true
        imageView.rightAnchor.constraint(equalTo: scrollView.rightAnchor).isActive = true
        imageView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor).isActive = true
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: Setup View
    func setUpUI() {
        let urlAvatar = URL(string: "https://api.tumblr.com/v2/blog/\(poster.blog_name!).tumblr.com/avatar")!
        let logoImageView = UIImageView.init()
        logoImageView.af_setImage(withURL: urlAvatar)
        logoImageView.frame = CGRect(x:0.0,y:0.0, width:40,height:40)
        logoImageView.layer.cornerRadius = 40 / 2.0
        logoImageView.layer.masksToBounds = true
        let imageItem = UIBarButtonItem.init(customView: logoImageView)
        let widthConstraint = logoImageView.widthAnchor.constraint(equalToConstant: 40)
        let heightConstraint = logoImageView.heightAnchor.constraint(equalToConstant: 40)
        heightConstraint.isActive = true
        widthConstraint.isActive = true
        navigationItem.rightBarButtonItem =  imageItem
    }
    
    
    //MARK:UIScrollViewDelegate
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
        if scrollView.zoomScale > 1 {
            
            if let image = imageView.image {
                
                let ratioW = imageView.frame.width / image.size.width
                let ratioH = imageView.frame.height / image.size.height
                
                let ratio = ratioW < ratioH ? ratioW:ratioH
                
                let newWidth = image.size.width*ratio
                let newHeight = image.size.height*ratio
                
                let left = 0.5 * (newWidth * scrollView.zoomScale > imageView.frame.width ? (newWidth - imageView.frame.width) : (scrollView.frame.width - scrollView.contentSize.width))
                let top = 0.5 * (newHeight * scrollView.zoomScale > imageView.frame.height ? (newHeight - imageView.frame.height) : (scrollView.frame.height - scrollView.contentSize.height))
                
                scrollView.contentInset = UIEdgeInsetsMake(top, left, top, left)
            }
        } else {
            scrollView.contentInset = UIEdgeInsets.zero
        }
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}

