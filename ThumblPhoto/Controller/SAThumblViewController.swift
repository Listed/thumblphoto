//
//  ViewController.swift
//  ThumblPhoto
//
//  Created by admin on 28.01.2018.
//  Copyright © 2018 SasinAndrey. All rights reserved.
//

import UIKit
import AlamofireImage

class SAThumblViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchResultsUpdating,UISearchBarDelegate {
    
    var posterInThumbl : NSMutableArray?
    var searchText : String?
    
    private let cellId = "cellId"
    
    lazy var tableview: UITableView = {
        
        let tableview = UITableView()
        
        tableview.backgroundColor = .lightGray
        tableview.delegate = self
        tableview.dataSource = self
        tableview.estimatedRowHeight = 60
        tableview.rowHeight = UITableViewAutomaticDimension
        tableview.register(SAThumblTableViewCell.self, forCellReuseIdentifier: self.cellId)
        return tableview
    }()
    
    //MARK: Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createSearchControllerForIos11()
        view.backgroundColor = .white
        view.addSubview(tableview)
        tableview.translatesAutoresizingMaskIntoConstraints = false
        setupAutoLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: SetupView
    
    private func createSearchControllerForIos11() {
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true // Navigation bar large titles
            navigationItem.title = "Search post"
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedStringKey.foregroundColor : UIColor.white]
            navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 0/255, green: 150/255, blue: 136/255, alpha: 1.0)
            
            let searchController = UISearchController(searchResultsController: nil) // Search Controller
            navigationItem.hidesSearchBarWhenScrolling = false
            navigationItem.searchController = searchController
            searchController.searchResultsUpdater = self
            
            searchController.searchBar.tintColor = UIColor.white
            UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self]).title = "Search"
            UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue: UIColor.white]
        }
    }
    
    
    private func setupAutoLayout() {
        
        tableview.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        tableview.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        tableview.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        tableview.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }
    
    //MARK: TableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if posterInThumbl != nil{
            return (posterInThumbl?.count)!
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableview.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! SAThumblTableViewCell
        
        if let post = self.posterInThumbl?.object(at: indexPath.row) as? SAThumblObjectParser{
            cell.labelUser.text = post.blog_name
            
            let urlAvatar = URL(string: "https://api.tumblr.com/v2/blog/\(post.blog_name!).tumblr.com/avatar")!
            let placeholderImageAvatar = UIImage(named: "noPhoto")!
            cell.imageViewAvatar.af_setImage(withURL: urlAvatar, placeholderImage: placeholderImageAvatar)
            
            let urlMainImage = URL(string: post.originalSize.object(forKey: "url") as! String)!
            let placeholderImageMain = UIImage(named: "noPhoto")!
            
            cell.mainImage.af_setImage(withURL: urlMainImage, placeholderImage: placeholderImageMain, filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .noTransition, runImageTransitionIfCached: false, completion: { (response) in
                if response.response != nil{
                    tableView.beginUpdates()
                    tableView.endUpdates()
                }
            })
            cell.labelDescription.text = post.summary
            
            cell.buttonRead.addTarget(self, action:#selector(actionRead), for: .touchUpInside)
            cell.buttonRead.tag = indexPath.row
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    //MARK: updateSearchResults
    
    func updateSearchResults(for searchController: UISearchController) {
        
        if !searchController.isActive {
            if let text = searchText{
                searchText = ""
                SANetworkManager().getRequestFromPost(textForSearch: text, completionHandler: { (responce) in
                    if((responce) != nil){
                        self.posterInThumbl = responce
                        
                        self.tableview.reloadData()
                        let indexPath = IndexPath(row: 0, section: 0)
                        self.tableview.scrollToRow(at: indexPath, at: .top, animated: true)
                        
                    }
                })
            }
        }else{
            searchText = searchController.searchBar.text
        }
    }
    
    //MARK: Actions
    @objc func actionRead(sender:UIButton) {
        let newViewController = SAImageViewController()
        newViewController.poster = posterInThumbl?.object(at: sender.tag) as! SAThumblObjectParser
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    
}

